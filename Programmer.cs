using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.Sql;

namespace BugTracker1
{
    public partial class Programmer : Form
    {
        String project_ID;
        string userID;
        SqlConnection conn = new SqlConnection(@"Data Source=(LocalDB)\v11.0;AttachDbFilename=F:\bugtracker\bug\BugTracker1\BugTracker1\bin\Debug\bugtracker.mdf;Integrated Security=True;Connect Timeout=30");


        public Programmer(string getemail)
        {
            InitializeComponent();
            userID = getemail;
        }

        private void button1_Click(object sender, EventArgs e)
        {

          
        }

        private void button4_Click(object sender, EventArgs e)
        {
            SqlCommand cmd = new SqlCommand("INSERT INTO product ([title],[desc],[status],[userID]) VALUES (@title,@desc,@status,@userID)", conn);
            string title = textBox1.Text;
            string desc = textBox2.Text;
            string status = comboBox1.Text;
            string ID = userID;
            cmd.Parameters.AddWithValue("@title", title);
            cmd.Parameters.AddWithValue("@desc", desc);
            cmd.Parameters.AddWithValue("@status", status);
            cmd.Parameters.AddWithValue("@userID", ID);
            conn.Open();
            int affectedrow = cmd.ExecuteNonQuery();
            if(affectedrow > 0)
            {

                MessageBox.Show("New Project Created. Go to Open Project to view");
                groupBox2.Enabled = false;
                conn.Close();
                
            }
            else
            {
                MessageBox.Show("Failed to insert. Try Again");
            }

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void Programmer_Load(object sender, EventArgs e)
        {
            groupBox2.Visible = false;
            groupBox1.Visible = false;
            groupBox3.Visible = false;
        }

        private void lOGOUTToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void newProjectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            groupBox2.Visible = true;
            groupBox2.Enabled = true;
            groupBox1.Visible = false;
            groupBox3.Visible = false;
            textBox1.Text = "";
            textBox2.Text = "";
            
        }

        private void openProjectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            groupBox1.Visible = true;
            groupBox1.Enabled = true;
            groupBox2.Enabled = false;
            groupBox2.Visible = false;
            groupBox3.Visible = false;
            string s = "SELECT * FROM product";
            conn.Open();
            SqlCommand cmd = new SqlCommand(s,conn);
            SqlDataReader x = cmd.ExecuteReader();
            while (x.Read())
            {
                comboBox2.Items.Add(x["title"].ToString());
                conn.Close();

               

            }
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            conn.Open();
            string s = "SELECT * FROM product WHERE title = '"+ comboBox2.Text +"'";
            SqlCommand cmd = new SqlCommand(s, conn);
            SqlDataReader x = cmd.ExecuteReader();
            while(x.Read())
            {
                textBox3.Text = x["desc"].ToString();
                label9.Text = x["status"].ToString();
                conn.Close();
                
                
            }
            
            
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            if (comboBox2.Text != "")
            {
                conn.Open();
                string s = "SELECT * FROM product WHERE title = '" + comboBox2.Text + "'";
                SqlCommand cmd = new SqlCommand(s, conn);
                SqlDataReader x = cmd.ExecuteReader();
                while (x.Read())
                {
                    project_ID = x["productID"].ToString();
                    MessageBox.Show(project_ID);
                    groupBox1.Enabled = false;
                    conn.Close();


                }

                groupBox3.Visible = true;

            }
            else
            {
                MessageBox.Show("Please select one project to add a bug");
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            SqlCommand cmd = new SqlCommand("INSERT INTO bug ([bugname],[ins],[severity],[platform],[product_ID]) VALUES (@title,@ins,@severity,@platform,@product_ID)", conn);
            string title = textBox4.Text;
            string ins = textBox5.Text;
            string severity = comboBox3.Text;
            string platform = comboBox4.Text;
            string productID = project_ID;

            cmd.Parameters.AddWithValue("@title", title);
            cmd.Parameters.AddWithValue("@ins", ins);
            cmd.Parameters.AddWithValue("@severity", severity);
            cmd.Parameters.AddWithValue("@platform", platform);
            cmd.Parameters.AddWithValue("@product_ID", productID);
            conn.Open();
            int affectedrow = cmd.ExecuteNonQuery();
            if (affectedrow > 0)
            {

                MessageBox.Show("Bug Added");
                productID = null;


            }
            else
            {
                MessageBox.Show("Failed to insert. Try Again");
            }
            conn.Close();
        }

        private void groupBox3_Enter(object sender, EventArgs e)
        {
            
        }

        private void lOGOUTToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            Main m = new Main();
            m.Show();
        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        
    }


    

  }


