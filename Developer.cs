using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.Sql;

namespace BugTracker1
{
    public partial class Developer : Form
    {
        string ID;
        SqlConnection conn = new SqlConnection(@"Data Source=(LocalDB)\v11.0;AttachDbFilename=F:\bugtracker\bug\BugTracker1\BugTracker1\bin\Debug\bugtracker.mdf;Integrated Security=True;Connect Timeout=30");

        public Developer()
        {
            InitializeComponent();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            
        }

        private void Developer_Load(object sender, EventArgs e)
        {
            dataGridView1.Visible = false;
            groupBox1.Visible = false;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void viewBugDetailsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            dataGridView1.Visible = true;
            conn.Open();
            string s = "SELECT product.title, bug.bugname, bug.severity FROM product INNER JOIN bug on product.productID = bug.product_ID INNER JOIN tbUser on tbUser.userID = product.userID";
            try
            {
                SqlDataAdapter data = new SqlDataAdapter(s, conn);
                DataTable dt = new DataTable();
                data.Fill(dt);
                BindingSource var = new BindingSource();
                var.DataSource = dt;
                dataGridView1.DataSource = var;
                data.Update(dt);
                
            }
            catch
            {
                MessageBox.Show("Failed to Show data. Contact Admin");
            }
            conn.Close();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            conn.Open();
            string m = "SELECT * FROM bug";
            SqlCommand m1 = new SqlCommand(m, conn);
            SqlDataReader dr = m1.ExecuteReader();
            while (dr.Read())
            {
                int i = e.RowIndex;
                DataGridViewRow row = dataGridView1.Rows[i];

                textBox2.Text = row.Cells["bugname"].Value.ToString();
                

            }
            conn.Close();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
                
        }

        private void viewAssignedBugToolStripMenuItem_Click(object sender, EventArgs e)
        {
            groupBox1.Visible = false;
            dataGridView1.Visible = true;
            conn.Open();
            string s = "SELECT product.title, bug.bugname, bug.severity, Assignto.AssignUser FROM product INNER JOIN bug on product.productID = bug.product_ID INNER JOIN tbUser on tbUser.userID = product.userID INNER JOIN Assignto on bug.bugID = Assignto.BugID";
            try
            {
                SqlDataAdapter data = new SqlDataAdapter(s, conn);
                DataTable dt = new DataTable();
                data.Fill(dt);
                BindingSource var = new BindingSource();
                var.DataSource = dt;
                dataGridView1.DataSource = var;
                data.Update(dt);
                
            }
            catch
            {
                MessageBox.Show("Failed to Show data. Contact Admin");
            }
            conn.Close();
        }

        private void viewNonAssignedBugToolStripMenuItem_Click(object sender, EventArgs e)
        {
            groupBox1.Visible = true;
            dataGridView1.Visible = true;
            conn.Open();
            string s = "SELECT product.title, bug.bugname, bug.severity FROM product INNER JOIN bug on product.productID = bug.product_ID INNER JOIN tbUser on tbUser.userID = product.userID WHERE bug.assignstatus = 1";
            
            try
            {
               

                SqlDataAdapter data = new SqlDataAdapter(s, conn);
                DataTable dt = new DataTable();
                data.Fill(dt);
                BindingSource var = new BindingSource();
                var.DataSource = dt;
                dataGridView1.DataSource = var;
                data.Update(dt);
                conn.Close();
           
            }
            catch
            {
                MessageBox.Show("Failed to Show data. Contact Admin");
            }
            

        
            conn.Open();
            string m = "SELECT * FROM tbUser WHERE type = 'Debugger'";
            SqlCommand m1 = new SqlCommand(m, conn);
            SqlDataReader dr = m1.ExecuteReader();
            while (dr.Read())
            {
                comboBox1.Items.Add(dr["username"].ToString());
            }
            conn.Close();
           
        }

        private void label2_Click_1(object sender, EventArgs e)
        {

        }

        private void aSSIGNToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            string bugname = textBox2.Text;
            string user = comboBox1.Text;
            if(bugname == "")
            {
                MessageBox.Show("Please select one bug");
            }
            else if(user == "")
            {
                MessageBox.Show("Please select one user to assign");
            }
            else
            {
                
                conn.Open();
                string s = "SELECT * FROM bug WHERE bugname = @bugname";

                SqlCommand cmd = new SqlCommand(s, conn);
                cmd.Parameters.AddWithValue("@bugname", bugname);
                SqlDataReader dr = cmd.ExecuteReader();
                while(dr.Read())
                {
                   ID = dr["bugID"].ToString();
                   MessageBox.Show(ID);
                   conn.Close();
                   
                    
                }
                
                conn.Open();
                string m = "INSERT INTO Assignto ([AssignUser],[BugID]) VALUES (@assignuser, @bugid)";
                SqlCommand c = new SqlCommand(m, conn);
                c.Parameters.AddWithValue("@assignuser", user);
                c.Parameters.AddWithValue("@bugid", ID);
                int row = c.ExecuteNonQuery();
                if(row > 0)
                {
                    MessageBox.Show("inserted");
                    conn.Close();
                    

                }
                
                conn.Open();
                string d = "UPDATE bug SET assignstatus = @data WHERE bugID = @bugid";
                SqlCommand q = new SqlCommand(d, conn);
                q.Parameters.AddWithValue("@data", "2");
                q.Parameters.AddWithValue("@bugid", ID);
                int rows = q.ExecuteNonQuery();
                if(rows > 0)
                {
                    MessageBox.Show("DATA UPDATED");
                    ID = null;
                }
                else
                {
                    MessageBox.Show("ERROR");
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            textBox2.Text = "";
        }

        private void lOGOUTToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            Main m = new Main();
            m.Show();
        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }
    }
}
