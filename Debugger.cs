using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.Sql;

namespace BugTracker1
{

    public partial class Debugger : Form
    {
        //Declare Global Variable
        SqlConnection conn;
        string userID;
        string ID;
        string getID;

        public Debugger(string ID)
        {
            InitializeComponent();
            //diable data view
            dataGridView1.Visible = false;
            userID = ID;

            //set get user ID
            DebuggerConn hey1 = new DebuggerConn();
            hey1.setLoginID(userID);
            getID = hey1.getID();

            //open connection
            conn = hey1.connect();

        }


        //View Bug Assigned and Display
        private void viewAssignedBugToolStripMenuItem_Click(object sender, EventArgs e)
        {
            dataGridView1.Visible = true;

            conn.Open();
            string s = "Select bug.bugname, bug.ins, bug.severity, bug.platform from bug JOIN Assignto ON Assignto.BugID = bug.bugID WHERE Assignto.AssignUser = @user";
            try
            {
                SqlCommand cmd = new SqlCommand(s, conn);
                cmd.Parameters.AddWithValue("@user", getID);
                SqlDataAdapter data = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                data.Fill(dt);
                BindingSource var = new BindingSource();
                var.DataSource = dt;
                dataGridView1.DataSource = var;
                data.Update(dt);
           

            }
            catch
            {
                MessageBox.Show("error");
               
            }
            conn.Close();

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            
            string m = "SELECT * FROM bug";
            SqlCommand m1 = new SqlCommand(m, conn);
            conn.Open();
            SqlDataReader dr = m1.ExecuteReader();
            
            while (dr.Read())
            {
                int i = e.RowIndex;
                DataGridViewRow row = dataGridView1.Rows[i];

                textBox1.Text = row.Cells["bugname"].Value.ToString();
                

            }
            conn.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string bugname = textBox1.Text;
            string comment = textBox2.Text;
            conn.Open();

                string s = "SELECT * FROM bug WHERE bugname = @bugname";

                SqlCommand cmd = new SqlCommand(s, conn);
                cmd.Parameters.AddWithValue("@bugname", bugname);
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    ID = dr["bugID"].ToString();
                    MessageBox.Show(ID);
                    conn.Close();

                }
                conn.Open();
                string m = "INSERT INTO comment ([Comment],[bugID]) VALUES (@comment, @bugid)";
                SqlCommand c = new SqlCommand(m, conn);
                c.Parameters.AddWithValue("@comment", comment);
                c.Parameters.AddWithValue("@bugid", ID);
                int row = c.ExecuteNonQuery();
                if (row > 0)
                {
                    MessageBox.Show("Solution Added");
                   


                }
                else
                {
                    MessageBox.Show("ERROR");
                }
                conn.Close();
            }

        private void Debugger_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
      
        }

       
    }


    public class DebuggerConn
    {
        SqlConnection conxn;

        public string loginID;

        public void setLoginID(string ID)
        {
            loginID = ID;
        }
        public string getID()
        {
            return loginID;
        }

        public SqlConnection connect()
        {
            return conxn = new SqlConnection(@"Data Source=(LocalDB)\v11.0;AttachDbFilename=F:\bugtracker\bug\BugTracker1\BugTracker1\bin\Debug\bugtracker.mdf;Integrated Security=True;Connect Timeout=30");

        }

       


    }
}

